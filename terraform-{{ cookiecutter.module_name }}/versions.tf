terraform {
  required_version = "{{ cookiecutter._terraform_version }}"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "{{ cookiecutter._aws_provider_version }}"
    }
  }
}
